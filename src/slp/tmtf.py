import enum

from .exceptions import DecodingError
from .clcw import CommunicationsLinkControlWord


FRAME_PRIMARY_HEADER_SIZE = 6
OCF_SIZE = 4
FECF_SIZE = 2


class FirstHeaderPointer(enum.IntEnum):
    NO_PACKET_START = 0b11111111111
    ONLY_IDLE_DATA = 0b11111111110


class TelemetryTransferFramePrimaryHeader:

    def __init__(
        self,
        spacecraft_id,
        vc,
        flag_ocf,
        mc_frame_count,
        vc_frame_count,
        flag_secondary_header,
        segment_length_id,
        first_header_pointer,
        frame_version=0b00,
        flag_synch=0b0,
        flag_packet_order=0b0,
    ):
        self.spacecraft_id = spacecraft_id
        self.vc = vc
        self.flag_ocf = flag_ocf
        self.mc_frame_count = mc_frame_count
        self.vc_frame_count = vc_frame_count
        self.flag_secondary_header = flag_secondary_header
        self.segment_length_id = segment_length_id
        self.first_header_pointer = first_header_pointer
        self.frame_version = frame_version
        self.flag_synch = flag_synch
        self.flag_packet_order = flag_packet_order

    def encode(self):
        data = bytearray()

        data.append(self.frame_version << 6 | self.spacecraft_id >> 4)
        data.append(
            (self.spacecraft_id & 0x0F) << 4 |
            self.vc << 1 |
            int(self.flag_ocf)
        )
        data.append(self.mc_frame_count)
        data.append(self.vc_frame_count)

        self.append(
            int(self.flag_secondary_header) << 7 |
            int(self.flag_synch) << 6 |
            int(self.packet_order_flag) << 5 |
            self.segment_length_id << 3 |
            self.first_header_pointer >> 8
        )

        self.append(self.first_header_pointer & 0xFF)

        return data

    @classmethod
    def decode(cls, data):

        if len(data) < FRAME_PRIMARY_HEADER_SIZE:
            raise DecodingError

        frame_version = data[0] >> 6

        spacecraft_id = (data[0] & 0x3F) << 4 | data[1] >> 4

        vc = (data[1] >> 1) & 0x07

        flag_ocf = data[1] & 0x01

        mc_frame_count = data[2]
        vc_frame_count = data[3]

        flag_secondary_header = bool(data[4] >> 7)
        flag_synch = bool((data[4] >> 6) & 0x01)
        flag_packet_order = bool((data[4] >> 5) & 0x01)
        segment_length_id = (data[4] >> 3) & 0x03

        first_header_pointer = (data[4] & 0x07) << 8 | data[5]

        return cls(
            spacecraft_id=spacecraft_id,
            vc=vc,
            flag_ocf=flag_ocf,
            mc_frame_count=mc_frame_count,
            vc_frame_count=vc_frame_count,
            flag_secondary_header=flag_secondary_header,
            segment_length_id=segment_length_id,
            first_header_pointer=first_header_pointer,
            frame_version=frame_version,
            flag_synch=flag_synch,
            flag_packet_order=flag_packet_order
        )


class TelemetryTransferFrameSecondaryHeader:

    def __init__(
        self,
        data_field,
        version=0b00,
    ):
        self.data_field = data_field
        self.version = version

    def __len__(self):
        return 1 + len(self.data_field)

    def encode(self):
        data = bytearray()

        length = len(self.data_field)
        data.append(self.version << 6 | length)

        data.append(self.data_field)
        return data

    @classmethod
    def decode(cls, data):

        version = data[0] >> 6
        length = data[0] & 0x3F

        if len(data) < length + 1:
            raise DecodingError

        data_field = data[1:length + 1]

        return cls(
            data_field,
            version
        )


class TelemetryTransferFrame:

    def __init__(
        self,
        primary_header,
        secondary_header=None,
        frame_data_field=None,
        operational_control_field=None,
        frame_error_control_field=None
    ):
        self.primary_header = primary_header
        self.secondary_header = secondary_header
        self.frame_data_field = frame_data_field
        self.operational_control_field = operational_control_field
        self.frame_error_control_field = frame_error_control_field

    def encode(self):
        data = bytearray()

        data.append(self.primary_header.encode())

        if self.secondary_header is not None:
            data.append(self.secondary_header.encode())

        if self.frame_data_field is not None:
            data.append(self.frame_data_field)

        if self.operational_control_field is not None:
            data.append(self.operational_control_field.encode())

        if self.frame_error_control_field is not None:
            data.append(self.frame_error_control_field)

        return data

    @classmethod
    def decode(cls, data, has_fecf):
        primary_header = TelemetryTransferFramePrimaryHeader.decode(data)

        # strip away primary header from data
        data = data[FRAME_PRIMARY_HEADER_SIZE:]

        # strip of frame error control field (if present)
        if has_fecf:
            data, frame_error_control_field =\
                data[:-FECF_SIZE], data[-FECF_SIZE:]
        else:
            frame_error_control_field = None

        # strip of operational control field (if present)
        if primary_header.flag_ocf:
            operational_control_field =\
                CommunicationsLinkControlWord.decode(data[-OCF_SIZE:])
            data = data[:-OCF_SIZE]
        else:
            operational_control_field = None

        if primary_header.flag_secondary_header:
            secondary_header =\
                TelemetryTransferFrameSecondaryHeader.decode(data)
            frame_data_field = data[len(secondary_header):]
        else:
            secondary_header = None
            frame_data_field = data

        return cls(
            primary_header=primary_header,
            secondary_header=secondary_header,
            frame_data_field=frame_data_field,
            operational_control_field=operational_control_field,
            frame_error_control_field=frame_error_control_field
        )
