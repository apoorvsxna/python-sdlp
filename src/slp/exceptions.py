

class DecodingError(Exception):
    pass


class EncodingError(Exception):
    pass
